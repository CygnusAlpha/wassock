"""
    Find the version
"""
import os
__version__ = open(
    os.path.join(os.path.abspath(os.path.dirname(__file__)), 'version.txt')
).read().strip()
