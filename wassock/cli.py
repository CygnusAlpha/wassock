#!/usr/bin/python
# 3.5
"""
wassock.cli

Usage:
  wassock
  wassock (-h | --help)
  wassock --version
Options:
  -h --help     Show this screen.
  --version     Show version.
"""

import logging

from docopt import docopt

import wassock

logging.basicConfig(
    level=logging.INFO,
    format='%(relativeCreated)6d %(threadName)s %(message)s'
)
LOG = logging.getLogger()


def run(arguments):
    """
        Run the module with the supplied arguments.
    """
    print(arguments)
    from wassock import server
    server.run()
    return 0


def main():
    """
        Main entrypoint.
    """
    arguments = docopt(__doc__, version=wassock.__version__)
    exit(run(arguments))
