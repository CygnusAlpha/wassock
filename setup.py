"""
    setup.py

    * python setup.py test
    * python setup.py install
    * python setup.py develop

    Required files:
        README.md
        mk_app/version.txt
        MANIFEST.in
        requirements.txt
        requirements_test.txt
"""
import os
import sys

from setuptools import find_packages, setup
from setuptools.command.test import test as TestCommand

# Mandatory project/module name
PROJECT = 'wassock'
MODULE = 'wassock'
KEYWORDS = "keywords"
URL = ""
# Optional entrypoint wassock.cli:main()
CLI = 'wassock'


class PyTest(TestCommand):
    """
        Implement setup.py test command
    """
    def initialize_options(self):
        TestCommand.initialize_options(self)
        # pylint: disable=attribute-defined-outside-init
        self.pytest_args = [
            '-s',
            '-ra',
            '--verbose',
            '--flake8',
            '--isort',
            '--pylint',
            '--cov-report=xml',
            '--cov-report=term-missing',
            '--cov={}'.format(MODULE),
            '--junitxml=junit.xml',
        ]

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        # pylint: disable=attribute-defined-outside-init
        self.test_suite = True

    def run_tests(self):
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)


def slurp(file):
    """
        Load text file relative to module
    """
    return open(os.path.join(os.path.abspath(os.path.dirname(__file__)), file)).read().strip()


REQUIREMENTS = slurp('requirements.txt').splitlines()
REQUIREMENTS_TEST = ['setuptools']
REQUIREMENTS_TEST.extend(slurp('requirements_test.txt').splitlines())
REQUIREMENTS_TEST.extend(REQUIREMENTS)

SETUP_CFG = {
    'name': PROJECT,
    'version': slurp(os.path.join(MODULE, "version.txt")),
    'license': "MIT",
    'description': slurp('README.md').splitlines()[3],
    'long_description': slurp('README.md'),
    'url': URL,
    'keywords': KEYWORDS,
    'packages': find_packages(),
    'py_modules': [MODULE],
    'cmdclass': {'test': PyTest},
    'tests_require': REQUIREMENTS_TEST,
    'install_requires': REQUIREMENTS,
    'dependency_links': [],
    'package_data': {
        '': ['*.txt', '*.md', '*.json'],
    },
    'python_requires': '>3.6',
    'classifiers': [
        # Development Status: 3 - Alpha, 4 - Beta, 5 - Production/Stable
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
}

if CLI:
    SETUP_CFG['entry_points'] = {
        'console_scripts': [
            '{} = {}.cli:main'.format(CLI, MODULE)
        ]
    }

setup(**SETUP_CFG)
