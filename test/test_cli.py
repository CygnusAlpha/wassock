"""
    Pytest BDD
"""
from pytest_bdd import given, scenarios, then, when

scenarios('cli.feature')


@given('a cli class')
def create_cli():
    """
        Create a cli instance
    """
    pass


@when('I call run')
def call_run():
    """
        Call run
    """
    pass


@then('I should get a return code 0')
def check_rc():
    """
        Check result
    """
    pass
